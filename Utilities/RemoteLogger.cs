﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OpenHardwareMonitor.Collections;
using OpenHardwareMonitor.Hardware;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace OpenHardwareMonitor.Utilities
{
    class RemoteLogger
    {
        private const string REMOTE_ACTION = "https://rigmonitoring.net/service/1.0/logHardware";

        private const string REMOTE_ACTION_GET_RIG_IDENTIFIER = "http://rigmonitoring.net/api/generate_rig_identifier";

        private const int CLIENT_VERSION = 3;
        
        private readonly IComputer computer;

        private readonly PersistentSettings settings;

        private DateTime lastLoggedTime = DateTime.MinValue;

        private WebClient webClient;

        public RemoteLogger(IComputer computer, PersistentSettings settings)
        {
            this.computer = computer;
            this.settings = settings;
            this.initWebClient();
            ServicePointManager.ServerCertificateValidationCallback += 
                new System.Net.Security.RemoteCertificateValidationCallback(bypassAllCertificateStuff);
        }

        private void initWebClient()
        {
            this.webClient = new WebClient();
            this.webClient.Encoding = System.Text.Encoding.UTF8;
        }

        private static bool bypassAllCertificateStuff(object sender, X509Certificate cert, X509Chain chain,
           System.Net.Security.SslPolicyErrors error)
        {
            return true;
        }

        public String generateRigIdentifier()
        {
            try
            {
                return webClient.DownloadString(REMOTE_ACTION_GET_RIG_IDENTIFIER);
            }
            catch (Exception e)
            {                
                logError("Failed to get rigidentifier from the remote server", e);
                return "";
            }
        }

        public bool Log(bool trace)
        {
            var now = DateTime.Now;

            if (lastLoggedTime + new TimeSpan(0, 0, 30) > now)
                return false;
           
            Rig rig = new Rig();
            rig.machineName = System.Environment.MachineName;
            rig.rigIdentifier = settings.GetValue("rigIdentifier", "");
            rig.time = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            rig.hardware = new List<IHardware>();
            rig.clientVersion = CLIENT_VERSION;

            for (int i = 0; i < computer.Hardware.Length; i++)
            {
                IHardware hardware = computer.Hardware[i];
                if (hardware.HardwareType == HardwareType.CPU ||
                    hardware.HardwareType == HardwareType.GpuAti ||
                    hardware.HardwareType == HardwareType.GpuNvidia ||
                    hardware.HardwareType == HardwareType.RAM)
                {
                    rig.hardware.Add(hardware);
                }
            }

            if (rig.rigIdentifier == null || rig.rigIdentifier == "")
            {
                logError("Rig identifier is not specified. Logging stopped", null);
                return true;
            }

            if (rig.hardware.Count <= 0)
            {
                logError("No hardware data found for logging. Logging stopped", null);
                return true;
            }

            string json = "";

            try
            {
                json = JsonConvert.SerializeObject(rig, Formatting.None, new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.All,
                    ContractResolver = new DynamicContractResolver(),
                    Converters = new List<Newtonsoft.Json.JsonConverter> {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                });

                if (trace)
                    logError(json, null);
            }
            catch (JsonSerializationException e)
            {
                logError("Failed to serialize hardware data. Logging stopped", e);
                return true;
            }

            try
            {
                webClient.CancelAsync();
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                webClient.UploadStringAsync(new Uri(REMOTE_ACTION), "POST", json);
            }
            catch (Exception e)
            {
                initWebClient();
                logError("Failed to send data to server", e);
                return false;
            }

            lastLoggedTime = now;

            return false;
        }

        private void logError(string errorMsg, Exception e)
        {
            try
            {
                System.IO.File.AppendAllText(Path.ChangeExtension(
                Application.ExecutablePath, ".logs"), "[" + DateTime.Now + "]" + errorMsg + "\n");
                if (e != null)
                    System.IO.File.AppendAllText(Path.ChangeExtension(
                        Application.ExecutablePath, ".logs"), "[" + DateTime.Now + "]" + e.ToString() + "\n");
            }
            catch (Exception ignore) { }            
        }
    }

    class Rig
    {
        public string rigIdentifier;

        public long time;

        public string machineName;

        public int clientVersion;

        public List<IHardware> hardware;
    }

    class DynamicContractResolver : DefaultContractResolver
    {
        //public new static readonly ShouldSerializeContractResolver Instance = new ShouldSerializeContractResolver();
 
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);
 
            if (property.PropertyName == "Values")
            {
                property.ShouldSerialize =
                    instance =>
                    {
                        return false;
                    };
            }
            return property;
        }
    }
}
